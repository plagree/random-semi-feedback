#-*-coding: utf-8 -*-

import numpy as np
from scipy.stats import bernoulli
from simulator import SimulatorRandomHorizon


class SimulatorIndependentPosition(SimulatorRandomHorizon):

    """
    Params:
    -------
    MAB: list
        List of arms.

    policies: list
        List of policies to test.

    L: int
        Number maximum of arms reviewed by a user (number of arms
        shown to the user).

    K: int
        Number of items (arms) in the pool.

    kappa: numpy array of float
        Probability that a position has been observed by a user (discrete
        probability of each position in the list).
    """

    def run(self, T, N, tsav):
        """Runs an experiment with parameters T and N.

        It returns a dictionary whose keys are policies and whose values
        are the regret obtained by these policies over the experiments and
        averaged over N runs.

        Parameters
        ----------
        T: int
            Length of the sequential allocations.

        N: int
            Number of Monte Carlo repetitions.
            
        tsav : a few hundred points within the horizon that must be saved for ploting
        """
        regret, Ni = dict(), dict()
        Nsub = np.size(tsav)
        taus_stats = np.zeros(T)
        

        for policy in self.policies:
            regret[policy.id()] = np.zeros(Nsub)
            if self.stats:
                Ni[policy.id()] = np.zeros((self.K, self.L, Nsub))

        for nExp in xrange(N):
            # Reinitialize the policy
            for policy in self.policies:
                policy.init(self.K)

            p_l = [bernoulli.rvs(self.kappa[i], size=T) for i in range(self.L)]
            p_l = [[p_l[l][t] for l in range(self.L)] for t in range(T)]

            for policy in self.policies:
                t0 = 0
                # List of rewards obtained by the policy / optimal choices
                rewards, optimalRewards = np.zeros(T), np.zeros(T)

                # Initialization
                if policy.recquiresInit():
                    for t in xrange(self.K):
                        # Initialization: we present lists
                        # A_t = [0, 1, 2], [1, 2, 3], ...
                        p_t = p_l[t]
                        A_t = [(t+i) % self.K for i in xrange(self.L)]
                        policy.nPlaysStatistics(A_t)
                        B_t = [a for a, i in zip(A_t, p_t) if i == 1]
                        roundRewards = self.MAB.play(B_t)
                        policy.updateState(B_t, roundRewards)
                        mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                        optimalRewards[t] = sum(mu_star * self.kappa)
                        rewards[t] = sum(mu_t * self.kappa)
                    t0 = self.K
                    policy.initialized = True

                # We follow the policy
                for t in xrange(t0, T):
                    p_t = p_l[t]
                    A_t = policy.selectArms(self.L)
                    policy.nPlaysStatistics(A_t)
                    B_t = [a for a, i in zip(A_t, p_t) if i == 1]
                    roundRewards = self.MAB.play(B_t)
                    policy.updateState(B_t, roundRewards)
                    mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                    optimalRewards[t] = sum(mu_star * self.kappa)
                    rewards[t] = sum(mu_t * self.kappa)
                regret[policy.id()] += optimalRewards[tsav] - rewards[tsav]
                if self.stats:
                    Ni[policy.id()] += policy.Ni

        for policy in self.policies:
            regret[policy.id()] = np.cumsum(regret[policy.id()]) / N
            if self.stats:
                for k in range(self.K):
                    for l in range(self.L):
                        Ni[policy.id()][k][l] = np.cumsum(Ni[policy.id()][k][l]) / N
        taus_stats /= N

        return regret, Ni, taus_stats
