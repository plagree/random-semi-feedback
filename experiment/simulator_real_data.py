#-*-coding: utf-8 -*-

import numpy as np
from scipy.stats import bernoulli


class SimulatorRealData(object):

    """
    Params:
    -------
    policies: list
        List of policies to test.

    L: int
        Number maximum of arms reviewed by a user (number of arms
        shown to the user).

    MAB:  EnvironmentRealData object
        For a given query key, the element is another dictionary of items, with
        a list of click / no click extracted from dataset.
        E.g. : {q1: {ad1: [0, 0, 1, 0, 1], ad2: [1, 1, 0, 0, 0, 0]} }
    """

    def __init__(self, policies, L, MAB):
        self.policies = policies
        self.L
        self.MAB = MAB

    def run(self, T, N):
        """Runs an experiment with parameters T and N.

        It returns a dictionary whose keys are policies and whose values
        are the regret obtained by these policies over the experiments and
        averaged over N runs.

        Parameters
        ----------
        T: int
            Length of the sequential allocations.

        N: int
            Number of Monte Carlo repetitions.
        """
        #regret, Ni = dict(), dict()
        #taus_stats = np.zeros(T)
        ctr = dict()

        for policy in self.policies:
            ctr[policy.id()] = np.zeros(T)

        for nExp in xrange(N):
            # Reinitialize the policy
            MAB.initializeQuery()
            for policy in self.policies:
                policy.init(MAB.K)

            # Stopping times for T rounds
            #taus = map(lambda x: x+1,
            #           np.random.choice(self.L, T, p=self.weights))
            #taus_stats += taus

            for policy in self.policies:
                t0 = 0
                # List of rewards obtained by the policy / optimal choices
                #rewards, optimalRewards = np.zeros(T), np.zeros(T)
                rewards = np.zeros(T)

                # Initialization
                if policy.recquiresInit():
                    for t in xrange(self.K):
                        # Initialization: we present lists
                        # A_t = [0, 1, 2], [1, 2, 3], ...
                        #tau_t = taus[t]
                        A_t = [(t+i) % MAB.K for i in xrange(self.L)]
                        policy.nPlaysStatistics(A_t)
                        """ TODO """
                        roundRewards = self.MAB.play(A_t[:tau_t]) # We play only tau_t arms from A_t
                        policy.updateState(A_t[:tau_t], roundRewards)
                        """ END TODO """
                        #mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                        #optimalRewards[t] = sum(mu_star * self.kappa)
                        #rewards[t] = sum(mu_t * self.kappa)
                        rewards[t] = sum(roundRewards)
                    t0 = self.K
                    policy.initialized = True

                # We follow the policy
                for t in xrange(t0, T):
                    #tau_t = taus[t]
                    A_t = policy.selectArms(self.L)
                    policy.nPlaysStatistics(A_t)
                    """ TODO """
                    roundRewards = self.MAB.play(A_t[:tau_t])
                    policy.updateState(A_t[:tau_t], roundRewards)
                    """ END TODO """
                    #mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                    #optimalRewards[t] = sum(mu_star * self.kappa)
                    rewards[t] = sum(roundRewards)
                #regret[policy.id()] += optimalRewards - rewards
                ctr[policy.id()] += rewards

        for policy in self.policies:
            ctr[policy.id()] = (ctr[policy.id()].cumsum() / np.arange(1, T)) / N

        return ctr
