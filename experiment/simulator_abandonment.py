#-*-coding: utf-8 -*-

import numpy as np
import sys
from scipy.stats import bernoulli
from simulator import SimulatorRandomHorizon


class SimulatorAbandonment(SimulatorRandomHorizon):

    """
    This simulator corresponds to the first model of the paper.

    Params:
    -------
    MAB: list
        List of arms.

    policies: list
        List of policies to test.

    L: int
        Number maximum of arms reviewed by a user (number of arms
        shown to the user).

    K: int
        Number of items (arms) in the pool.

    kappa: numpy array of float
        Probability that a position has been observed by a user (discrete
        probability of each position in the list).
    """

    def run(self, T, N, q, tsav):
        """Runs an experiment with parameters T and N.

        It returns a dictionary whose keys are policies and whose values
        are the regret obtained by these policies over the experiments and
        averaged over N runs.

        Parameters
        ----------
        T: int
            Length of the sequential allocations.

        N: int
            Number of Monte Carlo repetitions.

        q: int
            Quantile parameter (e.g. 25 -> quartiles)

        tsav: numpy array (ndim = 1)
            Points to save on each trajectory (a few points within the horizon)
        """
        regret, cumRegret, Ni = dict(), dict(), dict()
        Nsub = np.size(tsav)    # Number of points saved for each trajectory
        taus_stats = np.zeros(T)
        avgRegret, qRegret, QRegret = dict(), dict(), dict()

        for policy in self.policies:
            cumRegret[policy.id()] = np.zeros((N, Nsub))
            regret[policy.id()] = np.zeros(T)
            if self.stats:
                Ni[policy.id()] = np.zeros((self.K, self.L, Nsub))

        for nExp in xrange(N):
            if N < 10 or nExp % (N / 10) == 0:
                print nExp
                sys.stdout.flush()
            # Reinitialize the policy
            for policy in self.policies:
                policy.init(self.K)

            # Stopping times for T rounds
            taus = map(lambda x: x,
                       np.random.choice(self.L + 1, T, p=self.weights))
            taus_stats += taus

            for policy in self.policies:
                t = 0
                # List of rewards obtained by the policy / optimal choices
                rewards, optimalRewards = np.zeros(T), np.zeros(T)

                # Initialization
                if policy.recquiresInit():
                    k = 0
                    while k < self.K:
                        # Initialization: we present lists
                        # A_t = [0, 1, 2], [1, 2, 3], ...
                        tau_t = taus[t]
                        A_t = [(k+i) % self.K for i in xrange(self.L)]
                        policy.nPlaysStatistics(A_t)
                        roundRewards = self.MAB.play(A_t[:tau_t]) # We play only tau_t arms from A_t
                        policy.updateState(A_t[:tau_t], roundRewards)
                        mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                        optimalRewards[t] = sum(mu_star * self.kappa)
                        rewards[t] = sum(mu_t * self.kappa)
                        t += 1
                        if tau_t > 0:
                            k += 1
                    policy.initialized = True

                t0 = t
                # We follow the policy
                for t in xrange(t0, T):
                    tau_t = taus[t]
                    A_t = policy.selectArms(self.L)
                    policy.nPlaysStatistics(A_t)
                    roundRewards = self.MAB.play(A_t[:tau_t])
                    policy.updateState(A_t[:tau_t], roundRewards)
                    mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                    optimalRewards[t] = sum(mu_star * self.kappa)
                    rewards[t] = sum(mu_t * self.kappa)
                regret[policy.id()] += optimalRewards - rewards
                cumRegret[policy.id()][nExp] = np.cumsum(optimalRewards - rewards)[tsav]
                if self.stats:
                    Ni[policy.id()] += policy.Ni

        for policy in self.policies:
            regret[policy.id()] = np.cumsum(regret[policy.id()]) / N
            cumReg = cumRegret[policy.id()]
            avgRegret[policy.id()] = np.mean(cumReg, 0)
            qRegret[policy.id()] = np.percentile(cumReg, q, 0)
            QRegret[policy.id()] = np.percentile(cumReg, 100 - q, 0)
            if self.stats:
                for k in range(self.K):
                    for l in range(self.L):
                        Ni[policy.id()][k][l] = np.cumsum(Ni[policy.id()][k][l]) / N
        taus_stats /= N
        return regret, avgRegret, qRegret, QRegret, Ni, taus_stats
