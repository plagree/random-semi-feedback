#-*-coding: utf-8 -*-

import numpy as np
from scipy.stats import bernoulli


class SimulatorRandomHorizon(object):

    """
    Params:
    -------
    MAB: list
        List of arms.

    policies: list
        List of policies to test.

    L: int
        Number maximum of arms reviewed by a user (number of arms
        shown to the user).

    K: int
        Number of items (arms) in the pool.

    kappa: numpy array of float
        Probability that a position has been observed by a user (discrete
        probability of each position in the list).
    """

    def __init__(self, MAB, policies, L, K, kappa, stats=False):
        self.MAB = MAB
        self.policies = policies
        self.kappa = kappa
        self.stats = stats
        self.weights = np.zeros(L + 1)
        self.weights[-1] = self.kappa[-1]
        self.weights[0] = 1 - self.kappa[0]
        for l in range(1, L):
            self.weights[l] = self.kappa[l-1] - self.kappa[l]
        print self.weights
        self.L = L
        self.K = K

    def run(self, T, N, tsav):
        """Runs an experiment with parameters T and N.

        It returns a dictionary whose keys are policies and whose values
        are the regret obtained by these policies over the experiments and
        averaged over N runs.

        Parameters
        ----------
        T: int
            Length of the sequential allocations.

        N: int
            Number of Monte Carlo repetitions.
        """
        raise NotImplementedError("Method `run` is not implemented.")
