#-*-coding: utf-8 -*-

import numpy as np
from scipy.stats import bernoulli
from simulator import SimulatorRandomHorizon
import sys


class SimulatorAmbiguousReward(SimulatorRandomHorizon):

    """
    This model corresponds to the second model of the paper.

    Params:
    -------
    MAB: list
        List of arms.

    policies: list
        List of policies to test.

    L: int
        Number maximum of arms reviewed by a user (number of arms
        shown to the user).

    K: int
        Number of items (arms) in the pool.

    kappa: numpy array of float
        Probability that a position has been observed by a user (discrete
        probability of each position in the list).
    """

    def run(self, T, N, q, tsav):
        """Runs an experiment with parameters T and N.

        It returns a dictionary whose keys are policies and whose values
        are the regret obtained by these policies over the experiments and
        averaged over N runs.

        Parameters
        ----------
        T: int
            Length of the sequential allocations.

        N: int
            Number of Monte Carlo repetitions.

        q: int
            Quantile parameter (e.g. 25 -> quartiles)

        tsav: numpy array (ndim = 1)
            Points to save on each trajectory.
        """
        #Nsub = np.size(tsav)
        regret, cumRegret, N_plays = dict(), dict(), dict()
        Nsub = np.size(tsav)    # Number of points saved for each trajectory
        avgRegret, qRegret, QRegret = dict(), dict(), dict()

        for policy in self.policies:
            cumRegret[policy.id()] = np.zeros((N, Nsub))
            regret[policy.id()] = np.zeros(T)
            if self.stats:
                #Ni[policy.id()] = np.zeros((self.K, self.L, T))
                N_plays[policy.id()] = np.zeros((self.K, self.L))

        for nExp in xrange(N):
            if N < 10 or nExp % (N / 5) == 0:
                print "experiments executed: %d" % (nExp)
                sys.stdout.flush()
            # Reinitialize the policy
            for policy in self.policies:
                policy.init(self.K)

            p_l = [bernoulli.rvs(self.kappa[l], size=T) for l in range(self.L)]
            # List of samples of T observations for each of the L positions [T x L]
            kappas = np.array([[p_l[l][t] for l in range(self.L)]
                               for t in range(T)])

            for policy in self.policies:
                t0 = 0
                # List of rewards obtained by the policy / optimal choices
                rewards, optimalRewards = np.zeros(T), np.zeros(T)

                # Initialization
                if policy.recquiresInit():
                    t = t0
                    for k in range(self.K):
                        init_tab = np.zeros(self.L)
                        #while min(init_tab) == 0:
                        kappa_t = kappas[t]
                        A_t = [(k+l) % self.K for l in xrange(self.L)]
                        policy.nPlaysStatistics(A_t)
                        roundRewards = self.MAB.play(A_t)
                        #init_tab += roundRewards * kappa_t
                        # We update with the reward r_t = kappa_t * mu_t
                        policy.updateState(A_t, kappa_t * roundRewards)
                        mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                        optimalRewards[t] = sum(mu_star * self.kappa)
                        rewards[t] = sum(mu_t * self.kappa)
                        t += 1
                    t0 = t
                    policy.initialized = True

                # We follow the policy
                for t in xrange(t0, T):
                    # vector of observations (0 > observed, 1 > unobserved)
                    kappa_t = kappas[t]
                    A_t = policy.selectArms(self.L)
                    policy.nPlaysStatistics(A_t)
                    roundRewards = self.MAB.play(A_t)
                    policy.updateState(A_t, kappa_t * roundRewards)
                    mu_star, mu_t = self.MAB.getExpectedReward(A_t)
                    optimalRewards[t] = sum(mu_star * self.kappa)
                    rewards[t] = sum(mu_t * self.kappa)
                # regret contains cumulated regret for exp nExp at subsampled times
                regret[policy.id()] += optimalRewards - rewards
                cumRegret[policy.id()][nExp] += np.cumsum(optimalRewards - rewards)[tsav]
                if self.stats:
                    N_plays[policy.id()] += policy.N_plays

        for policy in self.policies:
            regret[policy.id()] = np.cumsum(regret[policy.id()]) / N
            cumReg = cumRegret[policy.id()]
            avgRegret[policy.id()] = np.mean(cumReg, 0)
            qRegret[policy.id()] = np.percentile(cumReg, q, 0)
            QRegret[policy.id()] = np.percentile(cumReg, 100 - q, 0)
            if self.stats:
                for k in range(self.K):
                    for l in range(self.L):
                        N_plays[policy.id()][k][l] = N_plays[policy.id()][k][l] / N

        return regret, avgRegret, qRegret, QRegret, N_plays
