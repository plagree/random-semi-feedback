#!/usr/bin/env python
#-*-coding: utf-8 -*-

import multiprocessing as mp
import numpy as np
from bandit import EnvironmentSyntheticData
from policy import PolicyThompsonSampling, PolicyPIE , PolicyKLUCB , PolicyUCB
from plot import plot_regret
from experiment import SimulatorAbandonment
import cPickle as pickle

# To Change: Which configs do you want for this experiment?
from config.config_model1_low import *

## PARAMETERS
T = 100000               # Finite Horizon
N = 10000                  # Monte Carlo simulations
statistics = False
# Save subsampled points for Figures
Nsub = 100
tsav = np.int_(np.logspace(2, np.log10(T - 1), Nsub))
# Choice of quantile display
q = 10

def experience(N):
    # Create an object of L arms (corresponds to model)
    mab = EnvironmentSyntheticData(K, kappa, params=params)
    # Policies to evaluate
    policies = [
            PolicyUCB(L, T, stats=statistics),
            PolicyThompsonSampling(L, T, stats=statistics),
            PolicyKLUCB(L, T, stats=statistics),
            PolicyPIE(L, T, stats=statistics),
            ]
    simulator = SimulatorAbandonment(mab, policies, L, K,
                                     kappa, stats=statistics)
    regret, avgRegret, qRegret, QRegret, N_plays, taus_stats = simulator.run(
            T, N, q, tsav)
    return regret, avgRegret, qRegret, QRegret, N_plays, taus_stats

def main(cpu_count=1):
    if cpu_count == 1:
        regret, avgRegret, qRegret, QRegret, N_plays, taus_stats = experience(N)
    else:
        n_exps = [N / cpu_count for i in range(cpu_count)]
        rest = N - (N / cpu_count) * cpu_count
        for i in range(rest):
            n_exps[i] += 1
        pool = mp.Pool()
        results = [pool.apply_async(experience, args=(i,)) for i in n_exps]
        results = [p.get() for p in results]
        regret, avgRegret, qRegret, QRegret = dict(), dict(), dict(), dict()
        N_plays = dict()
        taus_stats = np.zeros(T)
        for reg, avreg, qreg, Qreg, Ni, taus in results:
            taus_stats += taus
            for policy in avreg: # regret dictionary
                if policy not in avgRegret:
                    avgRegret[policy] = np.zeros(Nsub)
                    qRegret[policy] = np.zeros(Nsub)
                    QRegret[policy] = np.zeros(Nsub)
                    regret[policy] = np.zeros(T)
                if policy not in N_plays:
                    N_plays[policy] = np.zeros((K, L, T))
                avgRegret[policy] += avreg[policy]
                qRegret[policy] += qreg[policy]
                QRegret[policy] += Qreg[policy]
                regret[policy] += reg[policy]
                if statistics:
                    N_plays[policy] += Ni[policy]
        for policy in avgRegret:
            if statistics:
                N_plays[policy] /= cpu_count
            avgRegret[policy] /= cpu_count
            qRegret[policy] /= cpu_count
            QRegret[policy] /= cpu_count
            regret[policy] /= cpu_count
        taus_stats /= cpu_count
    lower_bound = EnvironmentSyntheticData(K, kappa, params=params).computeLowerBound(T, L)
    data = [[policy, regret[policy], avgRegret[policy], qRegret[policy],
             QRegret[policy]] for policy in avgRegret]
    return data, N_plays, taus_stats, lower_bound

def experiment_kappas(kappas, cpu_count=1, freq=10):
    """
    Not very beautiful but should work. To improve in the future.
    Don't forget to keep only one policy.
    """
    result = list()
    for _kappa in kappas:
        global kappa
        kappa = _kappa
        kappa_tup = tuple(k for k in kappa)
        data, N_plays, taus_stats, lower_bound = main(cpu_count)
        for algo, regret in data:
            k = '(%.3g, %.3g, %.3g)' % kappa_tup
            result.append([k, regret])
        with open('out/persistence_%s.pkl' % (str(kappa_tup)), 'wb') as foutput:
            pickle.dump(data, foutput, pickle.HIGHEST_PROTOCOL)
    plot_regret(result, "weird", kappa=True, log=False, freq=freq)
    plot_regret(result, "logweird", log=True, freq=freq,
                kappa=True, lower_bounds=[lower_bound])

if __name__ == '__main__':
    cpu_count = mp.cpu_count()
    result = dict()
    freq = 10
    #cpu_count = 1
    print "Number of CPUs: %d" % (cpu_count)
    ### EXPERIMENT ALGORITHMS ##
    data, N_plays, taus_stats, lower_bound = main(cpu_count)
    plot_regret(data, tsav, "regret_model1_synthetic", log=False, freq=freq,
                kappa=False, qtl=True)
    plot_regret(data, tsav, "regretlog_model1_synthetic", log=True, freq=freq,
                kappa=False, qtl=True, lower_bounds=[lower_bound])
    #with open('out/model1_low_synthetic.pkl', 'wb') as foutput:
    #    pickle.dump(data, foutput, pickle.HIGHEST_PROTOCOL)
    ## EXPERIMENT KAPPA ##
    #kappas = [np.ones(3), np.array([1, 0.75, 0.2]), np.array([1, 0.2, 0.1])]
    #experiment_kappas(kappas, cpu_count=cpu_count, freq=freq)
