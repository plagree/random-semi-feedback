#!/usr/bin/env python
#-*-coding: utf-8 -*-

import time
import multiprocessing as mp
import numpy as np
from bandit import EnvironmentSyntheticData, EnvironmentRealData
from policy import PolicyRSFKLUCB, PolicyAZUCB, PolicyBiasThompson, \
                   PolicyAmbiguousThompsonSampling, PolicyCKLUCB, \
                   PolicyDPIE, PolicyRankedBanditsUCB
from plot import plot_regret
from experiment import SimulatorAmbiguousReward
import cPickle as pickle

# To Change: Which configs do you want for this experiment?
from config.config_low import *

## PARAMETERS

T = 100000            # Finite Horizon
N = 10000             # Monte Carlo simulations
statistics = False
# save subsampled points for Figures
Nsub = 100
tsav = np.int_(np.logspace(2, np.log10(T - 1), Nsub))
#choice of quantile display
q = 10

def experience(N):

    # Create an object of L arms (corresponds to model)
    mab = EnvironmentSyntheticData(K, kappa, params=params)
    # Policies to evaluate
    policies = [
            #PolicyBiasThompson(L, T, kappa, stats=statistics),
            #PolicyAmbiguousThompsonSampling(L, T, kappa, stats=statistics),
            #PolicyRSFKLUCB(L, T, kappa, stats=statistics),
            #PolicyCKLUCB(L, T, kappa, stats=statistics),
            #PolicyAZUCB(L, T, kappa, stats=statistics),
            PolicyRankedBanditsUCB(L, T, kappa, stats=statistics),
            PolicyDPIE(L, T, kappa, stats=statistics),
            ]
    simulator = SimulatorAmbiguousReward(mab, policies, L, K,
                                         kappa, stats=statistics)
    regret, avgRegret, qRegret, QRegret, N_plays = simulator.run(T, N, q, tsav)
    return regret, avgRegret, qRegret, QRegret, N_plays

def main(cpu_count=1):
    if cpu_count == 1:
        regret, avgRegret, qRegret, QRegret, N_plays = experience(N)
    else:
        n_exps = [N / cpu_count for i in range(cpu_count)]
        rest = N - (N / cpu_count) * cpu_count
        for i in range(rest):
            n_exps[i] += 1
        pool = mp.Pool()
        results = [pool.apply_async(experience, args=(i,)) for i in n_exps]
        results = [p.get() for p in results]
        regret, avgRegret, qRegret, QRegret = dict(), dict(), dict(), dict()
        N_plays = dict()
        for reg, avreg, qreg, Qreg, Ni in results:
            for policy in reg: # regret dictionary
                if policy not in regret:
                    avgRegret[policy] = np.zeros(Nsub)
                    qRegret[policy] = np.zeros(Nsub)
                    QRegret[policy] = np.zeros(Nsub)
                    regret[policy] = np.zeros(T)
                if policy not in N_plays:
                    N_plays[policy] = np.zeros((K, L, T))
                avgRegret[policy] += avreg[policy]
                qRegret[policy] += qreg[policy]
                QRegret[policy] += Qreg[policy]
                regret[policy] += reg[policy]
                if statistics:
                    N_plays[policy] += Ni[policy]
        for policy in regret:
            if statistics:
                N_plays[policy] /= cpu_count
            avgRegret[policy] /= cpu_count
            qRegret[policy] /= cpu_count
            QRegret[policy] /= cpu_count
            regret[policy] /= cpu_count
    lower_bound = EnvironmentSyntheticData(
            K, kappa, params=params).computeAmbiguousLowerBound(T, L)
    data = [[policy, regret[policy], avgRegret[policy], qRegret[policy],
             QRegret[policy]] for policy in avgRegret]
    return data, N_plays, lower_bound

def experiment_kappas(kappas, cpu_count=1, freq=10):
    """
    Not very beautiful but should work. To improve in the future.
    Don't forget to keep only one policy.
    """
    result = list()
    for _kappa in kappas:
        global kappa
        kappa = _kappa
        kappa_tup = tuple(k for k in kappa)
        data, N_plays, taus_stats, lower_bound = main(cpu_count)
        for algo, regret in data:
            k = '(%.3g, %.3g, %.3g)' % kappa_tup
            result.append([k, regret])
        with open('out/persistence_%s.pkl' % (str(kappa_tup)), 'wb') as foutput:
            pickle.dump(data, foutput, pickle.HIGHEST_PROTOCOL)
    plot_regret(result, "weird", kappa=True, log=False, freq=freq)
    plot_regret(result, "logweird", log=True, freq=freq,
                kappa=True, lower_bounds=[lower_bound])


if __name__ == '__main__':
    cpu_count = mp.cpu_count()
    result = dict()
    freq = 10
    #cpu_count = 1
    print "Number of CPUs: %d" % (cpu_count)
    ### EXPERIMENT ALGORITHMS ##
    t = time.time()
    data, N_plays, lower_bound = main(cpu_count)
    elapsed = time.time() - t
    print "time spent: %d" % (elapsed)
    #plot_regret(data, tsav, "regretlog_rankedbandits_klucb", log=True, freq=freq,
    #            kappa=False, qtl=False, lower_bounds=[lower_bound])
    with open('out/model2_nips_pie_rba.pkl', 'wb') as foutput:
        pickle.dump(data, foutput, pickle.HIGHEST_PROTOCOL)
    #lower_bound = EnvironmentSyntheticData(
    #        K, kappa, params=params).computeAmbiguousLowerBound(T, L)
    #with open('out/model2_dpie2.pkl', 'rb') as fin:
    #    data = pickle.load(fin)
    #plot_regret(data, tsav, "regretlog_dpie2_quantile", log=True, freq=freq,
    #            kappa=False, qtl=True, lower_bounds=[lower_bound])
