#!/usr/bin/env python
#-*-coding: utf-8 -*-

import multiprocessing as mp
import numpy as np
from bandit import EnvironmentSyntheticData, EnvironmentRealData
from policy import PolicyRSFKLUCB, PolicyAZUCB, PolicyBiasThompson, \
                   PolicyAmbiguousThompsonSampling, PolicyCKLUCB, \
                   PolicyRankedBanditsUCB, PolicyDPIE
from plot import plot_regret
from experiment import SimulatorAmbiguousReward
import cPickle as pickle
import random
import time

# To Change: Which configs do you want for this experiment?
# from config.config_rsf3 import *

## PARAMETERS
T = 100000                   # Finite Horizon
N = 2000                       # Monte Carlo simulations
statistics = False
# save subsampled points for Figures
Nsub = 100
tsav = np.int_(np.logspace(2, np.log10(T - 1), Nsub))
#choice of quantile display
q = 10

# FOR REAL DATA EXPE #
L = 3

with open('matrix.pkl', 'rb') as fin:
    kdd2 = pickle.load(fin)
print kdd2
def generate_query(struct):
    q = random.choice(struct.keys())
    K = struct[q]['K']
    kappa = struct[q]['kappa']
    params = struct[q]['theta']
    return K, kappa, params
######################

def experience(N):
    kappa = None
    policies = [
            #PolicyBiasThompson(L, T, kappa, stats=statistics),
            #PolicyAmbiguousThompsonSampling(L, T, kappa, stats=statistics),
            #PolicyRSFKLUCB(L, T, kappa, stats=statistics),
            #PolicyCKLUCB(L, T, kappa, stats=statistics),
            #PolicyAZUCB(L, T, kappa, stats=statistics),
            PolicyRankedBanditsUCB(L, T, kappa, stats=statistics),
            PolicyDPIE(L, T, kappa, stats=statistics),
            ]
    regret, avgRegret = dict(), dict()
    qRegret, QRegret = dict(), dict()
    for pol in policies:
        regret[pol.id()] = np.zeros(T)
        avgRegret[pol.id()] = np.zeros(Nsub)
        qRegret[pol.id()] = np.zeros(Nsub)
        QRegret[pol.id()] = np.zeros(Nsub)
    for n in range(N):
        K, kappa, params = generate_query(kdd2)
        print kappa
        print params
        # Create an object of L arms (corresponds to model)
        mab = EnvironmentSyntheticData(K, kappa, params=params)
        # Policies to evaluate
        policies = [
                #PolicyBiasThompson(L, T, kappa, stats=statistics),
                #PolicyAmbiguousThompsonSampling(L, T, kappa, stats=statistics),
                #PolicyRSFKLUCB(L, T, kappa, stats=statistics),
                #PolicyCKLUCB(L, T, kappa, stats=statistics),
                #PolicyAZUCB(L, T, kappa, stats=statistics),
                PolicyRankedBanditsUCB(L, T, kappa, stats=statistics),
                ]
        simulator = SimulatorAmbiguousReward(mab, policies, L, K,
                                             kappa, stats=statistics)
        _regret, _avgRegret, _qRegret, _QRegret, N_plays = simulator.run(T, 1, q, tsav)
        for p in policies:
            regret[p.id()] += _regret[p.id()]
            avgRegret[p.id()] += _avgRegret[p.id()]
            qRegret[p.id()] += _qRegret[p.id()]
            QRegret[p.id()] += _QRegret[p.id()]
    for p in policies:
        regret[p.id()] /= N
        avgRegret[p.id()] /= N
        qRegret[p.id()] /= N
        QRegret[p.id()] /= N
    return regret, avgRegret, qRegret, QRegret

def main(cpu_count=1):
    if cpu_count == 1:
        regret, avgRegret, qRegret, QRegret = experience(N)
    else:
        n_exps = [N / cpu_count for i in range(cpu_count)]
        rest = N - (N / cpu_count) * cpu_count
        for i in range(rest):
            n_exps[i] += 1
        pool = mp.Pool()
        results = [pool.apply_async(experience, args=(i,)) for i in n_exps]
        results = [p.get() for p in results]
        regret, avgRegret = dict(), dict()
        qRegret, QRegret = dict(), dict()
        for reg, avgreg, qreg, Qreg in results:
            for policy in reg: # regret dictionary
                if policy not in regret:
                    avgRegret[policy] = np.zeros(Nsub)
                    qRegret[policy] = np.zeros(Nsub)
                    QRegret[policy] = np.zeros(Nsub)
                    regret[policy] = np.zeros(T)
                avgRegret[policy] += avgreg[policy]
                qRegret[policy] += qreg[policy]
                QRegret[policy] += Qreg[policy]
                regret[policy] += reg[policy]
        for policy in regret:
            avgRegret[policy] /= cpu_count
            qRegret[policy] /= cpu_count
            QRegret[policy] /= cpu_count
            regret[policy] /= cpu_count
    data = [[policy, regret[policy], avgRegret[policy], qRegret[policy],
             QRegret[policy]] for policy in regret]
    lower_bound = None
    return data, lower_bound

if __name__ == '__main__':
    cpu_count = mp.cpu_count()
    result = dict()
    freq = 10
    #cpu_count = 1
    print "Number of CPUs: %d" % (cpu_count)
    ### EXPERIMENT ALGORITHMS ##
    t = time.time()
    data, lower_bound = main(cpu_count)
    elapsed = time.time() - t
    print "time spent: %d" % (elapsed)
    #plot_regret(data, tsav, "regret_kdd2", log=False, freq=freq,
    #            kappa=False, qtl=False)
    #plot_regret(data, tsav, "regretlog_kdd2", log=True, freq=freq,
    #            kappa=False, qtl=False, lower_bounds=[])
    with open('out/kdd2_pie_rba.pkl', 'wb') as fout:
        pickle.dump(data, fout, pickle.HIGHEST_PROTOCOL)
    ## EXPERIMENT KAPPA ##
    #kappas = [np.ones(3), np.array([1, 0.75, 0.2]), np.array([1, 0.2, 0.1])]
    #experiment_kappas(kappas, cpu_count=cpu_count, freq=freq)
