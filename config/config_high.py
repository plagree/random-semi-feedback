#!/usr/bin/env python
#-*-coding: utf-8 -*-

import numpy as np

"""Configuration for Model 1 - High parameters."""

K = 5                       # Number of arms (items)
L = 3                       # Number of recommended items (among L >= K items)
kappa = np.array([0.9, 0.6, 0.3])# Probability of observing a position
params = [
        ("bernoulli", 0.95),
        ("bernoulli", 0.85),
        ("bernoulli", 0.75),
        ("bernoulli", 0.65),
        ("bernoulli", 0.55),
        ]
