#!/usr/bin/env python
#-*-coding: utf-8 -*-

import numpy as np

"""
Experiment for kappa impact
"""

K = 5                       # Number of arms (items)
L = 3                       # Number of recommended items (among L >= K items)
kappa = np.array([.9, 0.6, 0.3])
params = [
        ("bernoulli", 0.95),
        ("bernoulli", 0.95),
        ("bernoulli", 0.9),
        ("bernoulli", 0.85),
        ("bernoulli", 0.85),
        #("bernoulli", 0.7),
        ]
