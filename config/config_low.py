#!/usr/bin/env python
#-*-coding: utf-8 -*-

import numpy as np

"""Configuration for Model 1 - Low parameters."""

K = 5                       # Number of arms (items)
L = 3                       # Number of recommended items (among L >= K items)
kappa = np.array([0.9, 0.6, 0.3])# Probability of observing a position
params = [
        ("bernoulli", 0.45),
        ("bernoulli", 0.35),
        ("bernoulli", 0.25),
        ("bernoulli", 0.15),
        ("bernoulli", 0.05),
        ]
