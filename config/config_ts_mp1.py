#!/usr/bin/env python
#-*-coding: utf-8 -*-

import numpy as np

"""
Experiment 1 Komiyama et al. MP-TS 2015
"""

K = 5                       # Number of arms (items)
L = 2                       # Number of recommended items (among L >= K items)
kappa = np.array([1, 1])    # Probability of observing a position
params = [
        ("bernoulli", 0.7),
        ("bernoulli", 0.6),
        ("bernoulli", 0.5),
        ("bernoulli", 0.4),
        ("bernoulli", 0.3),
        ]
