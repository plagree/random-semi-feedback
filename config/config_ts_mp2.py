#!/usr/bin/env python
#-*-coding: utf-8 -*-

import numpy as np

"""
Experiment 2 Komiyama et al. MP-TS 2015
"""

K = 20                      # Number of arms (items)
L = 3                       # Number of recommended items (among L >= K items)
kappa = np.array([1, 1, 1]) # Probability of observing a position
params = [
        ("bernoulli", 0.15),
        ("bernoulli", 0.12),
        ("bernoulli", 0.1),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.05),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ("bernoulli", 0.03),
        ]
