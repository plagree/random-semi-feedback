import numpy as np
from scipy.stats import beta
from numpy.random import random

eps = 1e-3


def rejection_sampling(a, b, kappa):
    # Find maximal count
    i = np.argmax(a+b)
    a0 = a[i]
    b0 = b[i]
    kappa0 = kappa[i]

    # Find max of remainder terms
    a = np.delete(a, i)
    b = np.delete(b, i)
    kappa = np.delete(kappa, i)
    M = log_posterior_max(a, b, kappa)

    # Do rejection sampling, using draw from
    # exact distribution of maximal count as proposal
    theta = 1. + eps
    U = 1.
    p = 0.
    count = 0
    while U > p:
        count += 1
        theta = beta(a0 + 1, b0 + 1).rvs() / kappa0
        if theta > 1:
            continue
        p = np.exp(log_posterior(a, b, kappa, theta) - M)
        U = random()
    return theta

def log_posterior(a, b, kappa, theta):
    return sum(a * np.log(theta) + b * np.log(1. - kappa * theta))

def log_posterior_diff(a, b, kappa, theta):
    return sum(a / theta - (b * kappa) / (1. - kappa * theta))

def log_posterior_max(a, b, kappa):
    d =  log_posterior_diff(a, b, kappa, 1.)
    if d > 0:
        M = log_posterior(a, b, kappa, 1.)
    else:
        theta_M = 1.
        theta_m = 0.
        for i in range(20):
            if log_posterior_diff(a, b, kappa, (theta_m + theta_M) / 2) < 0:
                theta_M = (theta_m + theta_M) / 2
            else:
                theta_m = (theta_m + theta_M) / 2
        M = log_posterior(a, b, kappa, (theta_m + theta_M) / 2)
    return M
