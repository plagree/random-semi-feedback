cimport cython
import numpy as np
cimport numpy as np
import sys

from libc.math cimport log as c_log
from libc.math cimport abs as c_abs
from libc.stdio cimport printf

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double DELTA = 1e-8
cdef double EPS = 1e-6

cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b

cdef double KLBernoulli(double p, double q):
    if p >= 1 or q >= 1:
        return -1
    return p * c_log(p / q) + (1 - p) * c_log((1 - p) / (1 - q))

"""
This function keeps the lowest KLUCB per position (does not compute a KLUCB
based on all positions)
"""
cpdef np.ndarray[DOUBLE, ndim=1] _computeNaiveKLUCB(int n,
        np.ndarray[DOUBLE, ndim=2] gains,
        np.ndarray[DOUBLE, ndim=2] N_plays,
        np.ndarray[DOUBLE, ndim=1] kappa,
        int n_iter):
    cdef:
        Py_ssize_t i, j, l
        double upper_bound = 0
        double smallest_klucb = 0
        double p = 0, q = 0, f = 0
        cdef Py_ssize_t K = gains.shape[0]
        cdef Py_ssize_t L = gains.shape[1]
        np.ndarray[DOUBLE, ndim=1] res = np.zeros(
                K, dtype=np.float64)

    for i in range(K):
        smallest_klucb = 1
        for l in range(L):
            # with Bonferroni's correction
            upper_bound = (c_log(n) + c_log(L)) / N_plays[i][l]
            mu = double_max(gains[i][l] / (N_plays[i][l] * kappa[l]), DELTA)
            if mu >= 1:
                continue
            p = mu + EPS
            q = 1 - EPS
            for j in range(n_iter):
                _lambda = (p + q) / 2
                f = upper_bound - KLBernoulli(mu, _lambda)
                if f >= 0:
                    p = _lambda
                else:
                    q = _lambda
            if _lambda < smallest_klucb:
                smallest_klucb = _lambda
        res[i] = smallest_klucb
    return res
