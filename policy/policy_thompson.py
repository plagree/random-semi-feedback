#-*-coding: utf-8 -*-

import numpy as np
from policy import Policy
from distribution import BetaCollection
from scipy.stats import beta


class PolicyThompsonSampling(Policy):

    def __init__(self, L, T, stats):
        self.L = L
        self.T = T
        self.stats = stats

    def selectArms(self, L):
        """
        This functions selects the L arms among the K whose generated samples
        theta are the greatest.

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective samples
        """
        thetas = self.beta_collection.sample()
        theta_indices = list(thetas.argsort())  # Sort the indices
        output = theta_indices[::-1]            # Reverse list
        chosen_arms = output[:L]                # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for a, r in zip(arms, rewards):
            self.N_plays[a] += 1
            if r == 1:
                self.A_arms[a] += 1
            else:
                self.B_arms[a] += 1
            self.beta_collection.update(a, self.A_arms[a], self.B_arms[a])
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros(self.K)
        self.A_arms = np.ones(self.K)
        self.B_arms = np.ones(self.K)
        self.beta_collection = BetaCollection(self.A_arms, self.B_arms)
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'Thompson sampling without bias correction.'

    @staticmethod
    def id():
        return 'S-TS'

    @staticmethod
    def recquiresInit():
        return False
