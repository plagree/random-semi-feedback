cimport cython
import numpy as np
cimport numpy as np
from scipy import optimize
import sys

from libc.math cimport log as c_log
from libc.math cimport exp as c_exp
from libc.stdio cimport printf

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double EPS = 1e-6


cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b


cpdef double psi_lambda(double _lambda,
                        double mu_hat,
                        double x,
                        np.ndarray[DOUBLE, ndim=1] N_plays_l,
                        np.ndarray[DOUBLE, ndim=1] kappa):
    cdef:
        Py_ssize_t L = kappa.shape[0]
        Py_ssize_t l
        double res = 0
    for l in range(L):
        res += N_plays_l[l] * (- _lambda * mu_hat + c_log(1 - kappa[l] * x + \
                               kappa[l] * x * c_exp(_lambda / kappa[l])))
    return res

cpdef double psi(double x,
                 double mu_hat,
                 np.ndarray[DOUBLE, ndim=1] N_plays_l,
                 np.ndarray[DOUBLE, ndim=1] kappa,
                 double f_t):
    res = optimize.brent(psi_lambda,
                         args=(mu_hat, x, N_plays_l, kappa),
                         tol=1e-3)
    return - psi_lambda(res, mu_hat, x, N_plays_l, kappa) - f_t

cpdef np.ndarray[DOUBLE, ndim=1] _computeOURKLUCB(int t,
        np.ndarray[DOUBLE, ndim=2] gains,
        np.ndarray[DOUBLE, ndim=2] N_plays,
        np.ndarray[DOUBLE, ndim=1] kappa,
        int n_iter):
    cdef:
        Py_ssize_t k, l
        Py_ssize_t K = N_plays.shape[0]
        Py_ssize_t L = N_plays.shape[1]
        double a = 0
        double b = 1
        double mu_hat = 0
        double f_t = c_log(t)
        np.ndarray[DOUBLE, ndim=1] res = np.zeros(K, dtype=np.float64)

    for k in range(K):
        mu_hat = 0
        for l in range(L):
            mu_hat += gains[k][l] / kappa[l]
        mu_hat /= sum(N_plays[k])
        a = mu_hat
        if a >= b:
            res[k] = 1
            continue
        if psi(1, mu_hat, N_plays[k], kappa, f_t) < 0:
            res[k] = 1
            continue
        if psi(mu_hat, mu_hat, N_plays[k], kappa, f_t) > 0:
            res[k] = a
            continue
        res[k] = optimize.brentq(
                psi, a, b,
                args=(mu_hat, N_plays[k], kappa, f_t),
                xtol=1e-4)
    return res
