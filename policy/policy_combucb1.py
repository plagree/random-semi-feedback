#-*-coding: utf-8 -*-

import numpy as np
import sys
from policy import Policy

from _policy_combucb import _computeCombUCB1


class PolicyCombUCB1(Policy):

    def __init__(self, L, T, stats=False):
        self.L = L
        self.T = T
        self.stats = stats
        self.initialized = False

    @staticmethod
    def getSubsets(super_set, k, idx, current, solution):
        # Successful stop clause
        if len(current) == k:
            solution.append(current.copy());
            return
        # Unsuccessful stop clause
        if idx == len(super_set):
            return
        current.add(idx)
        # "guess" x is in the subset
        PolicyCombUCB1.getSubsets(super_set, k, idx+1, current, solution)
        current.remove(idx)
        # "guess" x is not in the subset
        PolicyCombUCB1.getSubsets(super_set, k, idx+1, current, solution)

    @staticmethod
    def getActions(k, n):
        solution = list()
        super_set = range(n)
        PolicyCombUCB1.getSubsets(super_set, k, 0, set(), solution)
        return solution

    def selectArms(self, L):
        """
        Select the best actions based on CombUCB1 index. Arms from this action
        are then ordered by decreasing empirical mean.

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms.
        """
        if not self.initialized:        # An arm hasn't been initialized
            return None                 # Better raise error
        n = self.t
        best_action = _computeCombUCB1(n, 20, self.gains, self.N_plays, self.actions)
        means = self.gains / self.N_plays
        action_means = np.zeros(self.K)
        for a in best_action:
            action_means[a] = means[a]
        mean_indices = list(action_means.argsort()) # Sort the indices
        output = mean_indices[::-1]                 # Reverse list
        chosen_arms = output[:L]                    # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for a, r in zip(arms, rewards):
            self.N_plays[a] += 1
            self.gains[a] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.actions = self.getActions(self.L, self.K)
        self.t = 0
        self.N_plays = np.zeros(self.K)         # Current count
        self.gains = np.zeros(self.K)
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))    # Statistics

    def __str__(self):
        return 'CombUCB1 policy'

    @staticmethod
    def id():
        return 'RSF-CombUCB1'

    @staticmethod
    def recquiresInit():
        return True
