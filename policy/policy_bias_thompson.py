#-*-coding: utf-8 -*-

import numpy as np
from policy import Policy
from distribution import BetaCollection
from scipy.stats import beta


class PolicyBiasThompson(Policy):
    """
    Bias Corrected Thompson Sampling (BC-MP-TS). This policy follows the
    ideas from the paper Optimal Regret Analysis of Thompson Sampling in
    Stochastic Multi-armed Bandit Problem with Multiple Plays (J. Komiyama
    et al., ICML 2015).
    """

    def __init__(self, L, T, kappa, stats=False):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective UCBs
        """
        thetas = self.beta_collection.sample()
        theta_indices = list(thetas.argsort())  # Sort the indices
        output = theta_indices[::-1]            # Reverse list
        chosen_arms = output[:L]                # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            self.N_plays[a][l] += 1
        N_tilde = (self.big_kappa * self.N_plays).sum(axis=1)
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            # UPDATE BIAS THOMPSON
            if r == 1:
                self.A_arms[a] += 1
            self.B_arms[a] = max(N_tilde[a] - self.A_arms[a]+1, 1.)
            self.beta_collection.update(a, self.A_arms[a], self.B_arms[a])
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros((self.K, self.L))     # Current count
        self.big_kappa = np.zeros((self.K,self.L))
        self.A_arms = np.ones(self.K)
        self.B_arms = np.ones(self.K)
        self.beta_collection = BetaCollection(self.A_arms, self.B_arms)
        for k in range(self.K):
            self.big_kappa[k] += self.kappa
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'Unbiased Thompson Sampling policy (Japanese TS)'

    @staticmethod
    def id():
        return 'BC-MP-TS'

    @staticmethod
    def recquiresInit():
        return False
