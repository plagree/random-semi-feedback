#-*-coding: utf-8 -*-


class Policy(object):

    def __init__(self, K, L, T, stats=False):
        """
        Constructor.
        """
        raise NotImplementedError("Method `__init__` is not implemented.")

    def selectArms(self, L):
        """
        This functions selects L arms among the K ones depending on statistics
        over the past observations.
        """
        raise NotImplementedError("Method `selectArms` is not implemented.")

    def updateState(self):
        """
        This function updates the statistics given the new observations.
        """
        raise NotImplementedError("Method `updateState` is not implemented.")

    @staticmethod
    def id():
        raise NotImplementedError("Static method `id` is not implemented.")

    @staticmethod
    def recquiresInit():
        raise NotImplementedError("Static method `recquiresInit` is not \
                                   implemented.")
