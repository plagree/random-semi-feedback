#-*-coding: utf-8 -*-

import numpy as np
import sys
from policy import Policy
from _policy_rsfklucb import _computeRSFKLUCB


class PolicyRSFKLUCB(Policy):

    def __init__(self, L, T, kappa, stats=False):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        This functions selects L arms among the K ones depending on their UCBs.
        This is the function to change if you want to analyse the importance
        of the UCB values in the recommended list on the final regret.

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective UCBs
        """
        if not self.initialized:        # An arm hasn't been initialized
            return None                 # Better raise error
        n = self.t
        self.UCBs = _computeRSFKLUCB(n, self.gains, self.N_plays,
                                     self.kappa, 20)   # Return the right L indices
        mixer = np.random.random(self.UCBs.size)
        UCB_indices = list(np.lexsort((mixer,self.UCBs))) # Sort the indices
        output = UCB_indices[::-1]              # Reverse list
        chosen_arms = output[:L]                # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            self.N_plays[a][l] += 1
            self.gains[a][l] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros((self.K, self.L))     # Current count
        self.gains = np.zeros((self.K, self.L))       # Rewards a every position
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))    # Statistics

    def __str__(self):
        return 'RSF-KL-UCB policy'

    @staticmethod
    def id():
        return 'RSF-KL-UCB'

    @staticmethod
    def recquiresInit():
        return True
