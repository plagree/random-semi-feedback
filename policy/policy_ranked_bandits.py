#-*-coding: utf-8 -*-

import numpy as np
from policy import Policy
from _policy_klucb import _computeKLUCB
import random
import sys


EPS = 1e-3

class PolicyRankedBanditsUCB(Policy):

    def __init__(self, L, T, kappa, stats=False):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        This functions selects L arms among the K ones depending on their UCBs.
        This is the function to change if you want to analyse the importance
        of the UCB values in the recommended list on the final regret.
        SO FAR: UCBs in reversed order

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective UCBs
        """
        if not self.initialized:
            return None     # Better raise error
        n = self.t
        #self.UCBs = self.gains / self.N_plays + \
        #        np.sqrt((1 + EPS) * np.log(n) / (2 * self.N_plays))
        #self.UCBs = _computeKLUCB(n, self.gains, self.N_plays, 20)
        chosen_arms = list()
        for l in xrange(L):
            self.UCBs = _computeKLUCB(n, self.gains[:,l], self.N_plays[:,l], 20)
            UCB_indices = list(self.UCBs.argsort()) # Sort the indices
            output = UCB_indices[::-1]              # Reverse list
            best_index = output[0]
            self.true_selection[l] = best_index
            if best_index in chosen_arms:
                chosen_arms.append(random.choice([i for i in xrange(self.K) \
                        if i not in chosen_arms]))
            else:
                chosen_arms.append(best_index)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            if self.initialized:
                self.N_plays[self.true_selection[l]][l] += 1
            else:
                self.N_plays[a][l] += 1
            if a == self.true_selection[l] or not self.initialized:
                self.gains[a][l] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.initialized = False
        self.N_plays = np.zeros((self.K, self.L))     # Current count
        self.gains = np.zeros((self.K, self.L))       # Rewards a every position
        self.true_selection = [-1] * self.L
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'RankedBandits with UCB policy of parameter 1/2'

    @staticmethod
    def id():
        return 'Ranked-UCB'

    @staticmethod
    def recquiresInit():
        return True
