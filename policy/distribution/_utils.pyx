cimport cython
import numpy as np
cimport numpy as np
import sys

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double EPS_MAX = 2.5-2
cdef double EPS_MIN = 1.5e-2
cdef int n_iter = 100
cdef int N_POINTS = 1000

cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b

cpdef np.ndarray[DOUBLE, ndim=1] vector_posterior_ts(
        np.ndarray[DOUBLE, ndim=1] x,
        np.ndarray[DOUBLE, ndim=2] plays,
        np.ndarray[DOUBLE, ndim=1] kappa):
    cdef:
        Py_ssize_t l
        int L = kappa.shape[0]
        int N = x.shape[0]
        np.ndarray[DOUBLE, ndim=1] res = np.ones(
                N, dtype=np.float64)
    for l in range(L):
        res *= x**(plays[l][0]-1)*(1-kappa[l]*x)**(plays[l][1]-1)
    return res

cpdef double scalar_posterior_ts(
        double x,
        np.ndarray[DOUBLE, ndim=2] plays,
        np.ndarray[DOUBLE, ndim=1] kappa):
    cdef:
        Py_ssize_t l
        Py_ssize_t L = kappa.shape[0]
        double res = 1
    for l in range(L):
        res *= x**(plays[l][0]-1)*(1-kappa[l]*x)**(plays[l][1]-1)
    return res

"""
Generates samples from posterior obtained by ambiguous feedback using grid
approximation + mass location
"""
cpdef np.ndarray[DOUBLE, ndim=1] _posterior_ts_sample(
        np.ndarray[DOUBLE, ndim=3] N_plays,
        np.ndarray[DOUBLE, ndim=1] theta_max,
        np.ndarray[DOUBLE, ndim=1] kappa,
        np.ndarray[DOUBLE, ndim=1] norm):
    cdef:
        Py_ssize_t k, i, l
        double p = 0, q = 0, m = 0, M = 0, f = 0, maximum = 0
        double left = 0, right = 0
        Py_ssize_t K = N_plays.shape[0]
        Py_ssize_t L = N_plays.shape[1]
        np.ndarray[DOUBLE, ndim=1] res = np.zeros(
                K, dtype=np.float64)
        np.ndarray[DOUBLE, ndim=1] points
        np.ndarray[DOUBLE, ndim=1] unorm_post
    for k in range(K):
        #print "k = %d" % (k)
        ## LEFT ##
        p = 0
        q = theta_max[k]
        m = scalar_posterior_ts(p, N_plays[k], kappa)
        maximum = scalar_posterior_ts(q, N_plays[k], kappa)
        M = maximum
        if p == q or maximum == 0:
            left = p
        elif m / maximum > EPS_MAX:
            left = p
        else:
            for i in range(n_iter):
                left = (p + q) / 2
                f = scalar_posterior_ts(left, N_plays[k], kappa)
                if f / maximum < EPS_MIN:
                    p = left
                elif f / maximum > EPS_MAX:
                    q = left
                else:
                    break
        ## RIGHT ##
        p = theta_max[k]
        q = 1
        maximum = scalar_posterior_ts(p, N_plays[k], kappa)
        m = scalar_posterior_ts(q, N_plays[k], kappa)
        M = maximum
        if p == q or maximum == 0:
            right = q
        elif m / maximum > EPS_MAX:
            right = q
        else:
            for i in range(n_iter):
                right = (p + q) / 2
                f = scalar_posterior_ts(right, N_plays[k], kappa)
                if f / maximum < EPS_MIN:
                    q = right
                elif f / maximum > EPS_MAX:
                    p = right
                else:
                    break
        #print 'Left = %.3f, right = %.3f' % (left, right)
        # We generate a sample
        points = np.linspace(left, right, N_POINTS, dtype=np.float64)
        unorm_post = vector_posterior_ts(points, N_plays[k], kappa)
        #print max(unorm_post)
        theta_max[k] = points[unorm_post.argmax()]
        normal_constant = sum(unorm_post) / N_POINTS
        norm[k] = normal_constant
        res[k] = np.random.choice(points, size=1, p=(unorm_post/sum(unorm_post)))
    #print theta_max
    return res

"""
Gull grid search -> pretty bad
"""
#cpdef np.ndarray[DOUBLE, ndim=1] _posterior_ts_sample(
#        np.ndarray[DOUBLE, ndim=3] N_plays,
#        np.ndarray[DOUBLE, ndim=1] theta_max,
#        np.ndarray[DOUBLE, ndim=1] kappa,
#        np.ndarray[DOUBLE, ndim=1] norm):
#    cdef:
#        Py_ssize_t k, i, l
#        double p = 0, q = 0, m = 0, M = 0, f = 0
#        double left = 0, right = 0
#        Py_ssize_t K = N_plays.shape[0]
#        Py_ssize_t L = N_plays.shape[1]
#        np.ndarray[DOUBLE, ndim=1] res = np.zeros(
#                K, dtype=np.float64)
#        np.ndarray[DOUBLE, ndim=1] points = np.linspace(0, 1, N_POINTS, dtype=np.float64)
#        np.ndarray[DOUBLE, ndim=1] unorm_post
#    for k in range(K):
#        # We generate a sample
#        unorm_post = vector_posterior_ts(points, N_plays[k], kappa)
#        theta_max[k] = points[unorm_post.argmax()]
#        normal_constant = sum(unorm_post) / N_POINTS
#        norm[k] = normal_constant
#        res[k] = np.random.choice(points, size=1, p=(unorm_post/sum(unorm_post)))
#    return res
#
#
#cdef double ratio(double g,
#                  np.ndarray[DOUBLE, ndim=1] kappa,
#                  np.ndarray[DOUBLE, ndim=2] N_plays_k):
#    cdef:
#        int L = kappa.shape[0]
#        Py_ssize_t l
#        double a = 0, b = 0, res = 1
#    for l in range(L):
#        a = N_plays_k[l][0]
#        b = N_plays_k[l][1]
#        if a > 1 and b > 1:
#            res *= (g*kappa[l]*(b-1)/(a-1))**(a-1)*((1-kappa[l]*g)*(a+b-2)/(b-1))**(b-1)
#        else:# b == 1 and a > 1:
#            res = g**(a-1)*(1-kappa[l]*g)**(b-1)
#        #elif
#    return res
#
#cpdef np.ndarray[DOUBLE, ndim=1] _posterior_ts_sample(
#        np.ndarray[DOUBLE, ndim=3] N_plays,
#        np.ndarray[DOUBLE, ndim=1] theta_max,
#        np.ndarray[DOUBLE, ndim=1] kappa,
#        np.ndarray[DOUBLE, ndim=1] norm):
#    cdef:
#        Py_ssize_t k, i, l
#        double u = 0, g = 0, r = 0
#        Py_ssize_t K = N_plays.shape[0]
#        np.ndarray[DOUBLE, ndim=1] res = np.zeros(
#                K, dtype=np.float64)
#    for k in range(K):
#        while True:
#            u = np.random.uniform()
#            g = np.random.uniform()
#            r = ratio(g, kappa, N_plays[k])
#            if r >= u:
#                res[k] = g
#                break
#    return res
