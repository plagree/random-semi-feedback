cimport cython
import numpy as np
cimport numpy as np
from numpy.random import random
from random import betavariate
import sys

from libc.math cimport log as c_log
from libc.math cimport exp as c_exp

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double EPS = 1e-6


cpdef double rejection_sampling(
        np.ndarray[DOUBLE, ndim=1] a,
        np.ndarray[DOUBLE, ndim=1] b,
        np.ndarray[DOUBLE, ndim=1] kappa):
    cdef:
        Py_ssize_t i
        Py_ssize_t l
        double a0 = 0
        double b0 = 0
        double kappa0 = 0
        double M = 0
        double theta_m = 0
        double theta_M = 0
        double theta = 1 + EPS
        double U = 1
        double p = 0
        int t = 0
        int count = 0
        int L = a.shape[0]
        np.ndarray[DOUBLE, ndim=1] a_copy = np.zeros(
                L - 1, dtype=np.float64)
        np.ndarray[DOUBLE, ndim=1] b_copy = np.zeros(
                L - 1, dtype=np.float64)
        np.ndarray[DOUBLE, ndim=1] kappa_copy = np.zeros(
                L - 1, dtype=np.float64)
    # Find maximal count
    i = np.argmax(a + b)
    a0 = a[i]
    b0 = b[i]
    kappa0 = kappa[i]

    # Find max of remainder terms
    for l in range(L-1):
        if l < i:
            a_copy[l] = a[l]
            b_copy[l] = b[l]
            kappa_copy[l] = kappa[l]
        elif l > 1:
            a_copy[l] = a[l+1]
            b_copy[l] = b[l+1]
            kappa_copy[l] = kappa[l+1]
    M = log_posterior_max(a_copy, b_copy, kappa_copy)

    # Do rejection sampling, using draw from
    # exact distribution of maximal count as proposal
    while U > p:
        count += 1
        theta = betavariate(a0 + 1, b0 + 1) / kappa0
        if theta > 1:
            continue
        p = c_exp(log_posterior(a_copy, b_copy, kappa_copy, theta) - M)
        U = random()
    return theta

cdef double log_posterior(np.ndarray[DOUBLE, ndim=1] a,
                          np.ndarray[DOUBLE, ndim=1] b,
                          np.ndarray[DOUBLE, ndim=1] kappa,
                          double theta):
    cdef:
        double res = 0
        Py_ssize_t l
        int L = a.shape[0]
    for l in range(L):
        res += a[l] * c_log(theta) + b[l] * c_log(1 - kappa[l] * theta)
    return res

cdef double log_posterior_diff(np.ndarray[DOUBLE, ndim=1] a,
                               np.ndarray[DOUBLE, ndim=1] b,
                               np.ndarray[DOUBLE, ndim=1] kappa,
                               double theta):
    cdef:
        double res = 0
        Py_ssize_t l
        int L = a.shape[0]
    for l in range(L):
        res += a[l] / theta - (b[l] * kappa[l]) / (1 - kappa[l] * theta)
    return res

cdef double log_posterior_max(np.ndarray[DOUBLE, ndim=1] a,
                              np.ndarray[DOUBLE, ndim=1] b,
                              np.ndarray[DOUBLE, ndim=1] kappa):
    cdef:
        Py_ssize_t i
        double d = 0
        double M = 0
        double theta_m = EPS
        double theta_M = 1 - EPS
    d =  log_posterior_diff(a, b, kappa, theta_M)
    if d > 0:
        M = log_posterior(a, b, kappa, theta_M)
    else:
        for i in range(20):
            if log_posterior_diff(a, b, kappa, (theta_m + theta_M) / 2) < 0:
                theta_M = (theta_m + theta_M) / 2
            else:
                theta_m = (theta_m + theta_M) / 2
        M = log_posterior(a, b, kappa, (theta_m + theta_M) / 2)
    return M
