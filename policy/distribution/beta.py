#-*-coding: utf-8 -*-

from random import betavariate
import numpy as np


class BetaCollection(object):

    def __init__(self, A, B):
        if len(A) != len(B):
            print 'Error in BetaDistribution alloc'   # Better raise exception
            return None
        self.distributions = [(a, b) for a, b in zip(A, B)]

    def sample(self):
        return np.array([betavariate(a, b) for a, b in self.distributions])

    def update(self, index, a, b):
        self.distributions[index] = (a, b)
