cimport cython
import numpy as np
cimport numpy as np
import sys

from libc.math cimport log as c_log
from libc.math cimport sqrt as c_sqrt
from libc.math cimport abs as c_abs
from libc.stdio cimport printf

ctypedef np.float64_t DOUBLE
ctypedef np.int32_t INT

np.import_array()

cdef double EPS = 1e-6

cdef inline double double_max(double a, double b): return a if a >= b else b
cdef inline double double_min(double a, double b): return a if a <= b else b

cdef double KLBernoulli(double p, double q):
    if p >= 1 or q >= 1:
        return -1
    return p * c_log(p / q) + (1 - p) * c_log((1 - p) / (1 - q))

cdef double g(double _lambda, double m, double v):
    cdef double lv = _lambda * v
    return (1 - lv + c_sqrt((1 - lv) * (1 - lv) + 4 * m * lv)) / 2

cdef double F(double _lambda,
              np.ndarray[DOUBLE, ndim=1] N_plays,
              np.ndarray[DOUBLE, ndim=1] theta_hat,
              int I_prime_size,
              I_prime):
    cdef:
        double result = 0
        double p = 0
        int i = 0
        int arm = 0
    for i in range(I_prime_size):
        arm = I_prime[i]
        p = theta_hat[arm]
        if p < EPS:
            p = EPS
        result += N_plays[arm] * KLBernoulli(p, g(_lambda, p, N_plays[arm]))
    return result


cpdef np.ndarray[DOUBLE, ndim=1] _computeCombUCB1(int n,
        int n_iter,
        np.ndarray[DOUBLE, ndim=1] gains,
        np.ndarray[DOUBLE, ndim=1] N_plays,
        actions):
    cdef:
        Py_ssize_t j
        int i = 0
        int best_action = 0
        int I_prime_size = 0
        int L = 0
        double current_best_index = 0
        double a = 0
        double b = 0
        double lbda = 0
        double f_n = c_log(n)
        double value = 0
        Py_ssize_t K = gains.shape[0]
        np.ndarray[np.float64_t, ndim=1] theta_hat = np.zeros(
                K, dtype=np.float64)

    for j in range(K):
        theta_hat[j] = gains[j] / N_plays[j]

    for i in range(len(actions)):
        L = len(actions[i])
        I_prime = [idx for idx in actions[i] if theta_hat[idx] < 1]
        I_prime_size = len(I_prime)
        if I_prime_size == 0:
            value = L
            if value > current_best_index:
                best_action = i
                current_best_index = value
            continue
        # Compute lambda such that F(lambda) = log(n) by binary search
        a = EPS
        b = 1 - EPS
        j = 0
        while j < n_iter:
            j += 1
            lbda = (a + b) / 2
            value = F(lbda, N_plays, theta_hat, I_prime_size, I_prime)
            if c_abs(value - f_n) < EPS:
                break
            if value > f_n:
                a = lbda
            else:
                b = lbda
        value = L - I_prime_size
        for j in range(I_prime_size):
            value += g(lbda, theta_hat[I_prime[j]], N_plays[I_prime[j]])
        if value > current_best_index:
            best_action = i
            current_best_index = value
    cdef np.ndarray[DOUBLE, ndim=1] res = np.zeros(
            L, dtype=np.float64)
    i = 0
    for a in actions[best_action]:
        res[i] = a
        i += 1
    return res
