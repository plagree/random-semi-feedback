#-*-coding: utf-8 -*-

import numpy as np
from policy import Policy
from distribution import rejection_sampling



class PolicyAmbiguousThompsonSampling(Policy):

    def __init__(self, L, T, kappa, stats):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats

    def selectArms(self, L):
        """
        This functions selects the L arms among the K whose generated samples
        theta are the greatest.

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective samples
        """
        #thetas = _posterior_ts_sample(self.N_plays, self.means, self.kappa,
        #                              self.norm)
        thetas = list()
        for a in range(self.K):
            thetas.append(rejection_sampling(self.N_plays[a,:,0],
                                             self.N_plays[a,:,1],
                                             self.kappa)
                         )
        thetas = np.array(thetas)
        theta_indices = list(thetas.argsort())  # Sort the indices
        output = theta_indices[::-1]            # Reverse list
        chosen_arms = output[:L]                # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            self.N_plays[a][l][0] += r
            self.N_plays[a][l][1] += 1 - r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros((self.K, self.L, 2))
        #self.means = 0.5 * np.ones(self.K)
        #self.norm = np.ones(self.K)
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'True Thompson sampling with grid sampling.'

    @staticmethod
    def id():
        return 'RSF-GRID-TS'

    @staticmethod
    def recquiresInit():
        return False
