#-*-coding: utf-8 -*-

import numpy as np
from scipy import optimize
import sys
from policy import Policy
from _policy_klucb import _computeKLUCB


class PolicyKLUCB(Policy):

    def __init__(self, L, T, stats=False):
        self.L = L
        self.T = T
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        This functions selects L arms among the K ones depending on their UCBs.
        This is the function to change if you want to analyse the importance
        of the UCB values in the recommended list on the final regret.
        SO FAR: UCBs in reversed order

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective UCBs
        """
        if not self.initialized:        # An arm hasn't been initialized
            return None                 # Better raise error
        n = self.t
        self.UCBs = _computeKLUCB(n, self.gains, self.N_plays, 20)
        mixer = np.random.random(self.UCBs.size)
        thetas = list(np.lexsort((mixer,self.UCBs)))    # Sort the indices
        output = thetas[::-1]                           # Reverse list
        chosen_arms = output[:L]                        # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for a, r in zip(arms, rewards):
            self.N_plays[a] += 1
            self.gains[a] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros(self.K)         # Current count
        self.gains = np.zeros(self.K)
        self.UCBs = np.zeros(self.K)
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))    # Statistics

    def __str__(self):
        return 'KL-UCB policy'

    @staticmethod
    def id():
        return 'S-KL-UCB'

    @staticmethod
    def recquiresInit():
        return True
