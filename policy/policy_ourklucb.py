#-*-coding: utf-8 -*-

import numpy as np
import sys
from policy import Policy

from _policy_ourklucb import _computeOURKLUCB


class PolicyOURKLUCB(Policy):

    def __init__(self, L, T, kappa, stats=False):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        Select the best actions based on CombUCB1 index. Arms from this action
        are then ordered by decreasing empirical mean.

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms.
        """
        if not self.initialized:        # An arm hasn't been initialized
            return None                 # Better raise error
        UCBs = _computeOURKLUCB(self.t, self.gains, self.N_plays, self.kappa, 20)
        mixer = np.random.random(UCBs.size)
        thetas = list(np.lexsort((mixer,UCBs))) # Sort the indices
        output = thetas[::-1]                       # Reverse list
        chosen_arms = output[:L]                    # Indices of chosen arms
        self.nPlaysStatistics(chosen_arms)
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            self.N_plays[a][l] += 1
            self.gains[a][l] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros((self.K, self.L))     # Current count
        self.gains = np.zeros((self.K, self.L))       # Rewards a every position
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'OURKLUCB policy'

    @staticmethod
    def id():
        return 'RSF-OURKLUCB'

    @staticmethod
    def recquiresInit():
        return True
