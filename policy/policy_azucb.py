#-*-coding: utf-8 -*-

import numpy as np
from policy import Policy


EPS = 1e-3

class PolicyAZUCB(Policy):

    def __init__(self, L, T, kappa, stats=False):
        self.L = L
        self.T = T
        self.kappa = kappa
        self.stats = stats
        self.initialized = False

    def selectArms(self, L):
        """
        This functions selects L arms among the K ones depending on their UCBs.
        This is the function to change if you want to analyse the importance
        of the UCB values in the recommended list on the final regret.
        SO FAR: UCBs in reversed order

        Output:
        -------
        chosen_arms: list of length L
            Indices of the chosen arms ordered by decreasing respective UCBs
        """
        if not self.initialized:
            return None     # Better raise error
        n = self.t
        N_tilde = (self.big_kappa * self.N_plays).sum(axis=1)
        self.UCBs = np.minimum(self.gains.sum(axis=1) / N_tilde + \
                np.sqrt(self.N_plays.sum(axis=1) \
                / N_tilde) * np.sqrt(np.log(n) / ((2 + EPS) * N_tilde)), 1)
        UCB_indices = list(self.UCBs.argsort()) # Sort the indices
        output = UCB_indices[::-1]              # Reverse list
        chosen_arms = output[:L]                # Indices of chosen arms
        #self.nPlaysStatistics(chosen_arms)     # done in simulator ?
        return chosen_arms

    def nPlaysStatistics(self, chosen_arms):
        if self.stats:
            for l, a in enumerate(chosen_arms):
                self.Ni[a][l][self.t] += 1

    def updateState(self, arms, rewards):
        for l, (a, r) in enumerate(zip(arms, rewards)): # position, (arm, reward)
            self.N_plays[a][l] += 1
            self.gains[a][l] += r
        self.t += 1

    def init(self, K):
        self.K = K
        self.t = 0
        self.N_plays = np.zeros((self.K, self.L))     # Current count
        self.gains = np.zeros((self.K, self.L))       # Rewards a every position
        self.big_kappa = np.zeros((self.K,self.L))
        for k in range(self.K):
            self.big_kappa[k] += self.kappa
        if self.stats:
            self.Ni = np.zeros((self.K, self.L, self.T))

    def __str__(self):
        return 'UCB policy with alpha = 3/4'

    @staticmethod
    def id():
        return 'RSF-UCB'

    @staticmethod
    def recquiresInit():
        return True
