# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 14:09:00 2016

@author: claire
small script that performs EM in order to learn kappa's and theta's
"""

import numpy as np
import cPickle as pickle

theta = np.array([0.7,0.5, 0.3, 0.1])
kappa = np.array([0.7, 0.5, 0.3])

# simulate data
data = np.zeros((4,3))
nb_obs = np.zeros((4,3))
for k in range(4):
    for l in range(3):
        N = np.random.poisson(100) #number of observations
        nb_obs[k][l] = N
        data[k][l] = np.random.binomial(N,theta[k]*kappa[l])

# EM algorithm
   #initialize
thetatemp = np.array([[0.5, 0.3, 0.2, 0.1]]) #0.5*np.ones((1,4))
kappatemp = np.array([[0.6,0.4,0.2]]) #0.1*np.ones((1,3))
thetac = np.zeros((1,4)) # current estimated parameter
kappac = np.zeros((1,3)) # ''
eps = 0.001

def EM(data, nb_obs, thetatemp, kappatemp, eps=0.001):
    K = np.size(thetainit)
    L = np.size(kappainit)
    thetac = np.zeros((1,K)) # current estimated parameter
    kappac = np.zeros((1,L)) # ''

    # e-step quantities
    x0 = np.zeros((K,L)) # conditionnal expectations
    y0= np.zeros((K,L))
    stop = 0

    #loop
    while (np.linalg.norm(thetatemp-thetac,2) > eps) and (stop < 1000):
        stop +=1
        thetac = thetatemp.copy()
        kappac = kappatemp.copy()
        x0 = np.multiply(thetac.transpose(), (1 - kappac)) \
                / (1 - np.multiply(thetac.transpose(), kappac))
        y0 = np.multiply(1 - thetac.transpose(), kappac) \
                / (1 - np.multiply(thetac.transpose(), kappac))

        for k in range(K):
            thetatemp[0,k] = (np.sum(data[k,:]) + np.sum(x0[k,:] * \
                    (nb_obs[k,:] - data[k,:]))) / np.sum(nb_obs[k,:])
        for l in range(L):
            #N_l = np.sum(nb_obs[k,:])
            kappatemp[0,l] = (np.sum(data[:,l]) + np.sum(y0[:,l] * \
                    (nb_obs[:,l] - data[:,l]))) / np.sum(nb_obs[:,l])
    return thetac, kappac

# import data
with open('out/matrix.pkl', 'rb') as f:
    data = pickle.load(f)                

parameters = dict()

for query in data.keys():
    thetalist, K, kappalist, nb_obs, clicks = data[query].values()
    thetainit = np.zeros((1,K))
    kappainit = np.zeros((1,np.size(kappalist)))
    kappainit[0] = kappalist
    for k in range(K):
        thetainit[0,k] = thetalist[k][1]
    parameters[query] = dict()
    thetac, kappac = EM(clicks, nb_obs, thetainit, kappainit)
    parameters[query]['theta'] = thetac
    parameters[query]['kappa'] = kappac

with open('out/parametersEM.pkl', 'wb') as foutput:
    pickle.dump(parameters, foutput, pickle.HIGHEST_PROTOCOL)
