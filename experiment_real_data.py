#!/usr/bin/env python
#-*-coding: utf-8 -*-

import multiprocessing as mp
import numpy as np
from bandit import MAB
from policy import PolicyUCB, PolicyThompsonSampling, PolicyKLUCB, PolicyPIE
from plot import plot_regret
from experiment import SimulatorAbandonment, SimulatorRealData
import cPickle as pickle


## PARAMETERS
T = 100000             # Finite Horizon
L = 3                   # Number of recommended items (among L >= K items)
N = 80                  # Monte Carlo simulations
statistics = False

def experience(N):
    # Create the object corresponding to the environment
    mab = EnvironmentRealData(queries)
    # Policies to evaluate
    policies = [
            PolicyUCB(L, T, stats=statistics),
            PolicyThompsonSampling(L, T, stats=statistics),
            PolicyKLUCB(L, T, stats=statistics),
            PolicyPIE(L, T, stats=statistics),
            ]
    simulator = SimulatorRealData(policies, L, mab)
    ctr = simulator.run(T, N)
    return ctr

def main(cpu_count=1):
    if cpu_count == 1:
        ctr = experience(N)
    else:
        n_exps = [N / cpu_count for i in range(cpu_count)]
        rest = N - (N / cpu_count) * cpu_count
        for i in range(rest):
            n_exps[i] += 1
        pool = mp.Pool()
        results = [pool.apply_async(experience, args=(i,)) for i in n_exps]
        results = [p.get() for p in results]
        #regret, N_plays = dict(), dict()
        #taus_stats = np.zeros(T)
        ctr = dict()
        #for reg, Ni, taus in results:
        for ctr in results:
            taus_stats += taus
            for policy in reg: # regret dictionary
                if policy not in ctr:
                    ctr[policy] = np.zeros(T)
                ctr[policy] += reg[policy]
        for policy in regret:
            ctr[policy] /= cpu_count
    return ctr

if __name__ == '__main__':
    cpu_count = mp.cpu_count()
    result = dict()
    freq = 10
    #cpu_count = 1
    print "Number of CPUs: %d" % (cpu_count)
    ctr = main(cpu_count)
    with open('out/persistence_ctr.pkl', 'wb') as foutput:
        pickle.dump(ctr, foutput, pickle.HIGHEST_PROTOCOL)
    plot_ctr(regret, "regret_kdd2", log=False, freq=freq, kappa=False)
    plot_ctr(regret, "regretlog_kdd2", log=True, freq=freq,
                kappa=False, lower_bounds=[])
