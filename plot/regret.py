#-*-coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

current_palette = sns.color_palette()
sns.set_style("ticks")
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc("lines", linewidth=3)
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)
matplotlib.rc('font', weight='bold')
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath} \boldmath"]

styles = ['o', '^', 's', 'D', '*']
colors = current_palette[0:4]+current_palette[5:6]
color_lb = current_palette[4]


algos = {'RSF-CKLUCB': 'D-M-KLUCB', 'BC-MP-TS': 'BC-MP-TS',
         'RSF-GRID-TS': 'D-TS', 'RSF-KL-UCB': 'D-BC-KLUCB',
         'RSF-UCB': 'D-UCB', 'D-PIE': 'D-PIE', 'Ranked-UCB': 'Ranked-UCB'}

def plot_regret(data, tsav, filename, log=False, kappa=False,
                freq=1, lower_bounds=None, qtl=False):
    fig = plt.figure(figsize=(7, 6))
    Nsub = len(tsav)
    markevery = max(1, Nsub / 12)
    T = len(data[0][1]) # length of regret
    N_curves = len(data)
    if log == True:
        plt.xscale('log')
        markevery = 1. / 12
        freq = 1
    i = 0
    if lower_bounds is not None:
        for index, lb in enumerate(lower_bounds):
            left = 0
            if qtl:
                left = 100
            plt.plot(np.arange(left, T)[0::freq], lb[left:][0::freq],
                     linestyle='--', linewidth=5.0, label=r"\textbf{Lower Bound}",
                     color=color_lb)
    for key, regret, avgRegret, qRegret, QRegret in data:
        T = len(regret)
        if kappa:
            label = r"$\boldsymbol{\kappa}$ = %s" % algos[key]
        elif qtl:
            label = r"\textbf{%s}" % algos[key]
            plt.plot(tsav, avgRegret, marker=styles[i],
                     markevery=markevery, ms=10.0, label=label, color=colors[i])
            plt.fill_between(tsav, qRegret, QRegret, alpha=0.15,
                             linewidth=1.5, color=colors[i])
        else:
            label = r"\textbf{%s}" % algos[key]
            plt.plot(np.arange(len(regret))[0::freq], regret[0::freq],
                     marker=styles[i], markevery=markevery, ms=10.0,
                     label=label, color=colors[i])
        i += 1

    plt.xlabel(r'Round $\boldsymbol{t}$', fontsize=20)
    plt.ylabel(r'Regret $\boldsymbol{R(T)}$', fontsize=18)
    if lower_bounds is not None:
        plt.legend(loc=2, fontsize=20).draw_frame(True)
    else:
        plt.legend(loc=4, fontsize=20).draw_frame(True)
    plt.savefig('out/%s.pdf' % (filename), bbox_inches='tight')
    plt.close(fig)
    return
