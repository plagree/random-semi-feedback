#-*-coding: utf-8 -*-

from arm import Arm
from scipy.stats import bernoulli


class ArmBernoulli(Arm):
    # Arm with Bernoulli distribution
    def __init__(self, p):
        self.bernoulli = bernoulli(p)
        self.mean = self.bernoulli.mean()   # = p
        self.var = self.bernoulli.var()     # = p(1-p)

    def pull(self):
        return self.bernoulli.rvs()

    def getExpectedReward(self):
        return self.mean

    def __str__(self):
        return 'Bernoulli arm of expectation %.2f' % (self.mean)
