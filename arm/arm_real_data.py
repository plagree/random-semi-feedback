#-*-coding: utf-8 -*-

from arm import Arm


class ArmPositionRealData(Arm):
    # Arm with clicks extracted from logs (from a Bernoulli distribution)
    def __init__(self, clicks, position):
        self.position = position
        self.clicks = np.array(clicks)
        self.mean = self.clicks.mean()
        self.var = self.clicks.var()

    def pull(self):
        return np.random.choice(self.clicks)

    def getExpectedReward(self):
        return self.mean

    def __str__(self):
        return 'Real data arm of empirical meann %.2f at position %d' \
                % (self.mean, self.position)


class ArmRealData(object):

    def __init__(self, positions):
        self.clicks = clicks

    def pull(self, position):
        return self.clicks[position].pull()

    def getExpectedReward(self, position):
        return self.clicks[position].getExpectedReward()
