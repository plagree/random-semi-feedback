#-*-coding: utf-8 -*-

from arm import Arm
import numpy as np


class ArmExp(Arm):
    # arm with truncated exponential distribution
    def __init__(self, _lambda):
        self._lambda = float(_lambda)
        self.mean = (1/float(_lambda))*(1-np.exp(-_lambda))   # = 1/lambda

    def pull(self):
        # Inverse Transform Sampling
        reward = min(-np.log(np.random.rand())/self._lambda, 1)
        return reward

    def getExpectedReward(self):
        return self.mean

    def __str__(self):
        return 'Truncated Exponential arm of expectation %.2f' % (self.mean)
