#-*-coding: utf-8 -*-

from arm import Arm
from scipy.stats import beta


class ArmBeta(Arm):
    # Arm with Beta distribution
    def __init__(self, a, b):
        self.beta = beta(a, b)
        self.mean = self.beta.mean()        # = a/(a+b)
        self.var = self.beta.var()          # = ab/((a+b)^2(a+b+1))

    def pull(self):
        return self.beta.rvs()

    def getExpectedReward(self):
        return self.mean

    def __str__(self):
        return 'Beta arm of expectation %.2f' % (self.mean)
